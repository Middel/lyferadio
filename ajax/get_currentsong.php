<?php
header('Access-Control-Allow-Origin: *');
if(isset($_GET['url'])) {
    $url = $_GET['url'];
    header('Content-type: text/xml');
    header('Pragma: public');
    header('Cache-control: private');
    header('Expires: -1');
    echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    echo '<title>';
    $title = getMp3StreamTitle($url,19200);
    echo $title;
    echo '</title>';
    exit;
} else {
    $url = $_POST['url'];
    $type = $_POST['type'];
}

switch ($type) {
    case 'direct':
        $title = getMp3StreamTitle($url,19200);
        echo $title;
        break;
    case 'shoutcast_v1':
        $url_p = parse_url($url);
        $url = $url_p['scheme']."://".$url_p['host'].":".$url_p['port']."/7.html";
        $ch = curl_init($url);
        curl_setopt_array($ch,array(CURLOPT_RETURNTRANSFER=>TRUE,CURLOPT_USERAGENT=>'Mozilla'));
        $response = curl_exec($ch);
        curl_close($ch);
        preg_match('#^.*<BODY>(.*)</BODY>.*$#i',$response,$matches);
        list($current,$status,$peak,$max,$reported,$bit,$song) = explode(',',$matches[1], 7);
        echo $song;
        break;
}

function getMp3StreamTitle($streamingUrl, $interval, $offset = 0, $headers = true)
{
    $needle = 'StreamTitle=';
    $ua = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36';
    $opts = [
        'http' => [
            'method' => 'GET',
            'timeout' => 2,
            'header' => 'Icy-MetaData: 1',
            'user_agent' => $ua
        ]
    ];
    if (($headers = get_headers($streamingUrl))) {
        foreach ($headers as $h) {
            if (strpos(strtolower($h), 'icy-metaint') !== false && ($interval = explode(':', $h)[1])) {
                break;
            }
        }
    }
    $context = stream_context_create($opts);
    if ($stream = fopen($streamingUrl, 'r', false, $context)) {
        $buffer = stream_get_contents($stream, $interval, $offset);
        fclose($stream);

        if (strpos($buffer, $needle) !== false) {
            $title = explode($needle, $buffer)[1];
            return substr($title, 1, strpos($title, ';') - 2);
        } else {
            return getMp3StreamTitle($streamingUrl, $interval, $offset + $interval, false);
        }
    } else {
        return "";
    }
}

function endsWith( $haystack, $needle ) {
    $length = strlen( $needle );
    if( !$length ) {
        return true;
    }
    return substr( $haystack, -$length ) === $needle;
}
