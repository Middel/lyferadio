<?php
/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ini_set('memory_limit','512M');
ini_set('max_execution_time',99999);
require_once(dirname(__FILE__) . '/ImageResize.php');
use \Gumlet\ImageResize;
$array_song = array();
$array_album = array();
require_once("getid3/getid3.php");
$getID3 = new getID3;
if(isset($_POST['directory'])) {
    $directory = $_POST['directory'];
} else {
    $directory = "songs";
}
if(isset($_POST['cache'])) {
    $cache = $_POST['cache'];
} else {
    $cache = 1;
}
if(isset($_POST['sort_by'])) {
    $sortBy = $_POST['sort_by'];
} else {
    $sortBy = 'nonw';
}
if(isset($_POST['order_by'])) {
    $orderBy = $_POST['order_by'];
} else {
    $orderBy = 'desc';
}
$scan = true;
if($cache==1 && file_exists(__DIR__ . '/../'.$directory.'/_songs.cache')) {
    $array_song = json_decode(file_get_contents(__DIR__ . '/../'.$directory.'/_songs.cache'),true);
    $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . '/../'.$directory.'/'), RecursiveIteratorIterator::SELF_FIRST);
    $count = 0;
    foreach ($objects as $name => $object) {
        $extension = strtolower(pathinfo($name, PATHINFO_EXTENSION));
        if ($object->isFile() && ($extension === 'mp3' || $extension === 'live')) {
            $count++;
        }
    }
    if($count!=count($array_song)) {
        $array_song=array();
        $scan=true;
        unlink(__DIR__ . '/../'.$directory.'/_songs.cache');
    } else {
        $scan=false;
    }
}

if($scan) {
    foreach (rglob(__DIR__ . '/../'.$directory.'/*.live',0,$sortBy,$orderBy) as $filename) {
        $subdirectory = dirname(substr($filename, strlen(__DIR__ . '/../' . $directory) + 1));
        $duration = -1;
        $json = @file_get_contents($filename);
        try {
            $array = json_decode($json,true);
        } catch (Exception $e) {
            continue;
        }
        $cover_name = preg_replace('"\.live$"', '.jpg', $filename);
        if(!file_exists($cover_name)) {
            $cover_name = 'img'.DIRECTORY_SEPARATOR.'default_cover_art.jpg';
        } else {
            $cover_name = $directory . DIRECTORY_SEPARATOR . $subdirectory . DIRECTORY_SEPARATOR . basename($cover_name);
        }
        $tmp = array();
        $tmp['name'] = htmlentities($array['name']);
        $tmp['artist'] = htmlentities($array['artist']);
        $tmp['album'] = "Live";
        $tmp['album_id'] = "00000000";
        $tmp['url'] = $array['url'];
        $tmp['live'] = true;
        if(isset($array['type'])) {
            $tmp['type'] = $array['type'];
        } else {
            $tmp['type'] = 'direct';
        }
        $tmp['cover_art_url'] = $cover_name;
        $tmp['duration'] = $duration;
        try {
            array_push($array_song,$tmp);
        } catch (Exception $e) {}
    }
    foreach (rglob(__DIR__ . '/../'.$directory.'/*.mp3',0,$sortBy,$orderBy) as $filename) {
        $subdirectory = dirname(substr($filename, strlen(__DIR__ . '/../' . $directory) + 1));
        $tmp = array();
        $mp3_tag = $getID3->analyze($filename);
        $duration = @$mp3_tag['playtime_string'];
        $title = @$mp3_tag['tags']['id3v2']['title'][0];
        $artist = @$mp3_tag['tags']['id3v2']['artist'][0];
        $album = @$mp3_tag['tags']['id3v2']['album'][0];
        $cover_name_path = preg_replace('"\.mp3$"', '.jpg', $filename);
        $link_file = preg_replace('"\.mp3$"', '.link', $filename);
        $lrc_file = preg_replace('"\.mp3$"', '.lrc', $filename);
        $cover_name = $directory . DIRECTORY_SEPARATOR . $subdirectory . DIRECTORY_SEPARATOR . basename($cover_name_path);
        if(!file_exists($cover_name_path)) {
            $image = getImage($mp3_tag);
            if($image!=false) {
                ob_start();
                imagejpeg($image);
                $jpg = ob_get_clean();
                $image_r = ImageResize::createFromString($jpg);
                $image_r->crop(500, 500, true, ImageResize::CROPCENTER);
                $image_r->save($cover_name_path);
            } else {
                $cover_name = 'img'.DIRECTORY_SEPARATOR.'default_cover_art.jpg';
            }
        }
        $filename = $directory . DIRECTORY_SEPARATOR . $subdirectory . DIRECTORY_SEPARATOR . basename($filename);
        if(empty($title)) $title = preg_replace('"\.mp3$"', '', basename($filename));
        if(empty($artist)) $artist = "--";
        if(empty($album)) $album = "--";
        if(file_exists($link_file)) {
            $tmp['link'] = file_get_contents($link_file);
            if (filter_var($tmp['link'], FILTER_VALIDATE_URL) === FALSE) {
                $tmp['link'] = '';
            }
        } else {
            $tmp['link'] = '';
        }
        if(file_exists($lrc_file)) {
            $lrc_file = $directory . DIRECTORY_SEPARATOR . $subdirectory . DIRECTORY_SEPARATOR . basename($lrc_file);
            $tmp['lrc'] = $lrc_file;
        } else {
            $tmp['lrc'] = '';
        }
        $tmp['name'] = htmlentities($title);
        $tmp['artist'] = htmlentities($artist);
        $tmp['album'] = htmlentities($album);
        $tmp['album_id'] = md5($album);
        $tmp['url'] = $filename;
        $tmp['live'] = false;
        $tmp['type'] = 'direct';
        $tmp['cover_art_url'] = $cover_name;
        $tmp['duration'] = $duration;
        try {
            array_push($array_song,$tmp);
        } catch (Exception $e) {}
    }
    if($cache==1) {
        $json = json_encode($array_song);
        file_put_contents(__DIR__ . '/../'.$directory.'/_songs.cache', $json);
    }
}

foreach ($array_song as $song) {
    $album_name = $song['album'];
    $artist_name = $song['artist'];
    $image = $song['cover_art_url'];
    $array_album = addAlbum($array_album, $album_name, $artist_name, $image);
}
echo json_encode(array("songs"=>$array_song,"albums"=>$array_album));

function rglob($pattern, $flags = 0, $sortBy = 'none', $order = 'desc') {
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir . '/' . basename($pattern), $flags, 'none', $order));
    }
    if ($sortBy === 'date') {
        $fileTimes = [];
        foreach ($files as $file) {
            $fileTimes[$file] = filemtime($file);
        }
        if ($order === 'asc') {
            asort($fileTimes);
        } else {
            arsort($fileTimes);
        }
        $files = array_keys($fileTimes);
    } elseif ($sortBy === 'name') {
        if ($order === 'asc') {
            sort($files, SORT_NATURAL | SORT_FLAG_CASE);
        } else {
            rsort($files, SORT_NATURAL | SORT_FLAG_CASE);
        }
    }
    return $files;
}

function getImage($file_info) {
    $artwork = null;
    if (isset($file_info['id3v2']['APIC'][0]['data'])) {
        $artwork = $file_info['id3v2']['APIC'][0]['data'];
    } else {
        if (isset($file_info['comments']['picture'][0]['data'])) {
            $artwork = $file_info['comments']['picture'][0]['data'];
        }
    }
    if (!$artwork) {
        return null;
    }
    return imagecreatefromstring($artwork);
}

function addAlbum($albums, $album_name, $artist_name, $image) {
    foreach ($albums as &$album) {
        if ($album['album'] === $album_name) {
            foreach ($album['artists'] as $artist) {
                if ($artist === $artist_name) {
                    return $albums;
                }
            }
            $album['artists'][] = $artist_name;
            return $albums;
        }
    }
    $albums[] = ['album' => $album_name, 'album_id'=> md5($album_name), 'image' => $image, 'artists' => [$artist_name]];
    return $albums;
}
