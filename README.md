# LyfeRadio

LyfeRadio has been developed entirely in PHP, HTML, and JQUERY.
It's simple and intuitive: Just select the song you wanna listen to and enjoy!

## FEATURES:
- Fetch MP3 songs from a song directory.
- Fetch title, album, artist, and image directly from the MP3 files.
- Automatic waveform generator.
- Live stream support.
- Fetch current song title and album from live streams.
- Search feature.
- Completely Responsive.

## REQUIREMENTS:
- PHP 7+